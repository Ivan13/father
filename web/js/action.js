$(function ()
{
    var nav = $('#js-nav');
    var mobileNav = $('#js-mobile-menu');

    nav.on('click', 'li', function () {
        nav.find('li').removeClass('active');
        $(this).addClass('active');
        if (mobileNav.is(':visible')) {
            mobileNav.click();
        }
    });
    
    $('section').mouseover(function () {
        var id = $(this).attr('id');
        var a = nav.find('a[href="#' + id + '"]');

        if (id == 'slider') {
            nav.find('li').removeClass('active');
            nav.find('li:first').addClass('active');
        } else if (a.length > 0) {
            nav.find('li').removeClass('active');
            a.closest('li').addClass('active');
        }
    });
    
    mobileNav.on('click', function () {
        $($(this).data('target')).toggle();
    });

    $('form[data-sform]').sForm();
});

