$.fn.sForm = function () {

    /* -- submit button class -- */
    function SubmitButton(form) {
        this.form = form;
        this.button = $(form.attr('data-submit-btn'));

        this.params = {};
    }

    SubmitButton.prototype.processing = function () {
        var $this = this;

        $.each($this.button, function (k, button) {
            $this.params[$(button).attr('id')] = $(button).html();
            $(button).html('Идет отправка...');
        });
    };

    SubmitButton.prototype.initial = function () {
        var $this = this;

        $.each($this.button, function (k, button) {
            $(button).html($this.params[$(button).attr('id')]);
        });
    };
    /* -- end submit button class -- */



    /* -- form action -- */
    var $self = $(this);
    var formId = $self.attr('id');
    var clearTimeId = null;
    console.log(formId + ' - init sForm');

    $self.on('submit', function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        console.log(formId + ' - submit sForm');

        if ($self.attr('data-busy')) {
            return;
        }

        var submitButton = new SubmitButton($self);

        clearTimeout(clearTimeId);
        clearResponse();

        $.ajax({
            url       : $self.attr('action'),
            type      : "post",
            data      : $self.serialize(),
            dataType  : 'json',
            beforeSend: function () {
                $self.attr('data-busy', true);
                submitButton.processing();
            }
        }).done(function (response) {
            var responseBlock;
            if (response.success) {
                responseBlock = '.success-form';
                $self[0].reset();
            } else {
                responseBlock = '.error-form';
            }

            $self.find(responseBlock).html(response.message);
            clearTimeId = setTimeout(function () {
                clearResponse();
            }, 5000)

        }).always(function (response) {
            $self.attr('data-busy', null);
            submitButton.initial();
        });
    });

    function clearResponse() {
        $self.find('.error-form').empty();
        $self.find('.success-form').empty();
    }
    /* -- form action end -- */
};