<?php

$file_db_local = __DIR__ . '/db-local.php';

if (file_exists($file_db_local)) {
    return require $file_db_local;
}

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=domservice',
    'username' => 'root',
    'password' => '11235813function',
    'charset' => 'utf8',
];
