<?php
/**
 * Created: 20.05.18
 */

namespace app\helpers;


use Yii;

class Cookie
{
    public static function has(string $name): bool
    {
        $cookies = Yii::$app->request->cookies;
        if ($cookies->has($name)) {
            return true;
        }

        return false;
    }

    public static function send(array $params = [])
    {
        $cookies = Yii::$app->response->cookies;

        if (isset($params['name'], $params['value'])) {
            $cookies->add(new \yii\web\Cookie($params));
        }
    }
}