<?php

namespace app\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "user_request".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 * @property string $is_new
 */
class UserRequest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_request';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value'      => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['created_at', 'updated_at', 'is_new'], 'safe'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'created_at' => 'Создана',
            'is_new' => 'Новая'
        ];
    }

    public static function getNumberNewRequests()
    {
        return self::find()->where(['=', 'is_new', 1])->count();
    }

    public static function findAllByEmail($email)
    {
        return self::findAll(['email' => $email]);
    }
}
