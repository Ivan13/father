<?php

namespace app\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "user_tracker".
 *
 * @property int $id
 * @property string $ip
 * @property string $created_at
 */
class UserTracker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_tracker';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value'      => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['created_at'], 'safe'],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'created_at' => 'Created At',
        ];
    }

    public static function getNumberVisitorsPerDay()
    {
        $date_time = new \DateTime('now', new \DateTimeZone('Asia/Novosibirsk'));
        $mysql_date_time = $date_time->format('Y-m-d') . ' 00:00:00';

        return self::find()->where(['>', 'created_at', $mysql_date_time])->count();
    }
}
