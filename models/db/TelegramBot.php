<?php

namespace app\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "telegram_bot".
 *
 * @property int $id
 * @property int $chat_id
 * @property int $is_verify
 * @property int $attempts
 * @property string $response
 * @property int $created_at
 * @property int $updated_at
 */
class TelegramBot extends \yii\db\ActiveRecord
{
    const COMMAND = [
        '/login',
    ];

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telegram_bot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id'], 'required'],
            [['chat_id', 'attempts', 'created_at', 'updated_at'], 'integer'],
            [['response'], 'string'],
            [['is_verify'], 'integer', 'max' => 1],
            [['chat_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'is_verify' => 'Is Verify',
            'attempts' => 'Attempts',
            'response' => 'Response',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @param array $data
     */
    public static function reply($data)
    {
        $model = self::findOne([
            'chat_id' => ArrayHelper::getValue($data, 'message.chat.id'),
        ]);

        $message = ArrayHelper::getValue($data, 'message.text');

        if ($model === null) {
            self::create($data);
        } elseif ($model->attempts > 10) {
            return;
        } elseif (self::isCommand($message)) {
            self::sendAnswerByCommand($data, $model);
        } elseif ($model->attempts % 2 === 1) {
            $model->login($data);
        } else {
            self::sendAnswerByCommand($data, $model);
        }
    }

    /**
     * @param array $data
     */
    protected function login($data)
    {
        $chat_id  = ArrayHelper::getValue($data, 'message.chat.id');
        $message = ArrayHelper::getValue($data, 'message.text');

        if ($message === Yii::$app->params['telegram']['token']) {
            $this->is_verify = 1;
            $this->attempts = 0;
            if ($this->save()) {
                Yii::$app->telegram->sendMessage([
                    'chat_id' => ArrayHelper::getValue($data, 'message.chat.id'),
                    'text' => 'Вы успешно авторизованы. Теперь вы будете получать уведомления о новых запросах пользователей с сайта https://jaluzi-barnaul.ru/',
                ]);

                return;
            }
        }

        Yii::$app->telegram->sendMessage([
            'chat_id' => $chat_id,
            'text' => 'Упс... Что-то пошло не так.',
        ]);
    }

    /**
     * @param array $data
     * @param self $model
     */
    protected static function sendAnswerByCommand($data, $model)
    {
        $chat_id  = ArrayHelper::getValue($data, 'message.chat.id');
        $message = ArrayHelper::getValue($data, 'message.text');

        if ((boolean) $model->is_verify === true) {
            Yii::$app->telegram->sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Вы уже авторизованы. Больше нет необходимости писать в этот чат.',
            ]);

            return;
        }

        if ($message === '/login') {
            Yii::$app->telegram->sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Пожалуйста, пришлите мне секретный токен для авторизации.',
            ]);

            if ($model !== null) {
                $model->attempts += 1;
                $model->save();
            }

            return;
        }

        Yii::$app->telegram->sendMessage([
            'chat_id' => $chat_id,
            'text' => 'Пожалуйста, авторизуйтесь.'
        ]);
    }

    /**
     * @param string $message
     * @return bool
     */
    protected static function isCommand($message)
    {
        return in_array($message, self::COMMAND);
    }

    /**
     * @param array $data
     * @return TelegramBot|null
     */
    protected static function create($data)
    {
        $model = new self();
        $model->chat_id = ArrayHelper::getValue($data, 'message.chat.id');
        $model->response = Json::encode($data);

        if ($model->save()) {
            return $model;
        }

        return null;
    }
}
