<?php
/**
 * Created: 22.04.18
 */

namespace app\models\form;


use app\models\db\TelegramBot;
use app\models\db\UserRequest;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class UserRequestForm extends Model
{
    public $name;
    public $phone;
    public $email;

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['phone'], 'match', 'pattern' => '/^[0-9\-\s\+()]*$/i', 'message' => 'Пожалуйста, в поле "Ваш телефон" используйте только цифры и пробелы.'],
            [['email'], 'email'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Ваш телефон',
            'email' => 'Ваш Email',
            'submit' => 'Отправить!',
        ];
    }

    public function getError()
    {
        $error = $this->getFirstErrors();
        return array_shift($error);
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->isSpam()) {
            return true;
        }

        $user_request = new UserRequest();
        $user_request->name = $this->name;
        $user_request->phone = $this->phone;
        $user_request->email = $this->email;
        $user_request->is_new = 1;

        if ($user_request->save()) {
            $this->sendNotifyToTelegram();

            $this->name = null;
            $this->phone = null;
            $this->email = null;

            return true;
        }

        return false;
    }

    public function sendNotifyToTelegram()
    {
        /** @var TelegramBot[] $telegramUsers */
        $telegramUsers = TelegramBot::findAll(['is_verify' => true]);
        foreach ($telegramUsers as $user) {
            Yii::$app->telegram->sendMessage([
                'chat_id' => $user->chat_id,
                'parse_mode' => 'HTML',
                'text' => "<b>НОВАЯ ЗАЯВКА!</b>\nИмя: <i>{$this->name}</i>\nТелефон: <i>{$this->phone}</i>\nEmail: {$this->email}",
            ]);
        }
    }

    private function isSpam()
    {
        $result = false;

        if (!$this->email) {
            return $result;
        }

        if (preg_match('/[A-Za-z]/', $this->name)) {
            return true;
        }

        /** @var UserRequest[] $requestWithSameEmail */
        $requestWithSameEmail = UserRequest::findAllByEmail($this->email);

        foreach ($requestWithSameEmail as $key => $previousRequests) {
            $result = $result || $previousRequests->name !== $this->name;
            $result = $result || $previousRequests->phone !== $this->phone;
        }

        return $result;
    }
}
