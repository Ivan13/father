<?php
/**
 * Created: 10.10.2020
 */

namespace app\controllers;


use app\models\db\TelegramBot;
use DateTime;
use DateTimeZone;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;

class TelegramController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     *  Экшен устанавливает урл для вебхук
     */
    public function actionSetWebhook()
    {
        $url = "https://jaluzi-barnaul.ru" . Url::to(['telegram/webhook']);

        echo "<pre>";
        print_r(array_merge((array) Yii::$app->telegram->setWebhook(['url' => $url]), [
            'url' => $url,
        ]));
        echo "</pre>";
    }

    public function actionWebhook()
    {
        $body = Yii::$app->request->getRawBody();
        $data = Json::decode($body);
        $this->logToFile("\n\nIncome: \n" . print_r($data, 1), 'webhook');

        TelegramBot::reply($data);
    }

    protected function logToFile($message, $prefix)
    {
        $log_dir = Yii::getAlias('@runtime') . '/logs/telegram';

        if (!is_dir($log_dir)) {
            mkdir($log_dir);
        }

        $date = new DateTime('now', new DateTimeZone('Asia/Novosibirsk'));
        $log = $log_dir . '/' . $prefix . '_' . $date->format('m-Y') . '.log';
        $message = "\n\n" . "[" . $date->format('Y-m-d H:i:s') . "]" . "\n" . $message;
        $result = $fp = fopen($log, 'a');

        if ($fp) {
            $result = fwrite($fp, $message);
            fclose($fp);
        }

        return (boolean) $result;
    }
}
