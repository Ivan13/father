<?php
/**
 * Created: 03.06.18
 */

namespace app\controllers;


use app\models\form\UserRequestForm;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class FormController extends Controller
{
    public function behaviors()
    {
        return [
            'bootstrap' => [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'user-request'  => ['post'],
                ],
            ],
        ];
    }

    public function actionUserRequest()
    {
        $user_request = new UserRequestForm();
        if ($user_request->load(Yii::$app->request->post()) && $user_request->save()) {
            return ['success' => true, 'message' => 'Заявка успешна отправлена!'];
        }

        return ['success' => false, 'message' => $user_request->getError()];
    }
}
