<?php
/**
 * Created: 15.04.18
 */

namespace app\controllers;

use app\models\db\UserRequest;
use app\models\form\LoginForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'user-request'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['user-request'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUserRequest()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserRequest::find()->orderBy(['id' => SORT_DESC]),
        ]);

        return $this->render('user_request', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUserRequestProcess($id)
    {
        $user_request = UserRequest::findOne(['id' => $id]);
        if ($user_request !== null) {
            $user_request->is_new = 0;
            $user_request->save();
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['/admin/request']);
    }
}