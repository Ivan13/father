<?php

namespace app\controllers;

use app\behaviors\UserTrackerBehavior;
use app\models\db\UserRequest;
use app\models\db\UserTracker;
use app\models\form\UserRequestForm;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => UserTrackerBehavior::class,
                'model_class' => \app\models\db\UserTracker::class,
            ]
        ];

    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user_request = new UserRequestForm();

        if (!Yii::$app->user->isGuest) {
            $number_new_requests = UserRequest::getNumberNewRequests();
            $number_visitors_per_day = UserTracker::getNumberVisitorsPerDay();
        }

        $number_new_requests = $number_new_requests ?? false;
        $number_visitors_per_day = $number_visitors_per_day ?? false;

        return $this->render('index', [
            'user_request' => $user_request,
            'number_new_requests' => $number_new_requests,
            'number_visitors_per_day' => $number_visitors_per_day,
        ]);
    }
}
