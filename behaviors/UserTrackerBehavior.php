<?php
/**
 * Created: 20.05.18
 */

namespace app\behaviors;

use app\helpers\Cookie;
use Yii;
use yii\base\Behavior;
use yii\web\Controller;

class UserTrackerBehavior extends Behavior
{
    public $model_class;

    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    public function beforeAction($event)
    {
        if (Yii::$app->user->isGuest && Cookie::has('return') === false) {
            $model = new $this->model_class();
            $model->ip = $this->getUserIP();
            if ($model->save()) {
                Cookie::send(['name' => 'return', 'value' => 1, 'expire' => time() + 86400 * 365]);
            }
        }
    }

    private function getUserIP()
    {
        return Yii::$app->getRequest()->getRemoteIP();
    }
}