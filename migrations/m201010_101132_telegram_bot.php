<?php

use yii\db\Migration;

/**
 * Class m201010_101132_telegram_bot
 */
class m201010_101132_telegram_bot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%telegram_bot}}', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->bigInteger()->unique()->notNull(),
            'is_verify' => $this->boolean()->defaultValue(false),
            'attempts' => $this->integer()->defaultValue(0),
            'response' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%telegram_bot}}');
    }
}
