<?php

use yii\db\Migration;

/**
 * Class m180520_035551_table_user_tracker
 */
class m180520_035551_table_user_tracker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_tracker}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_tracker}}');
    }
}
