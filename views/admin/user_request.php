<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="group-promo-code-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row">
            <div class="col-sm-2"><a href="/" class="btn btn-info">Назад</a></div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'rowOptions' => function (\app\models\db\UserRequest $model, $key, $index, $grid) {
                        if ($model->is_new) {
                            return ['style' => 'background-color: #A0D6B4;'];
                        }
                        return [];
                    },
                    'columns' => [
                        'name',
                        'phone',
                        'email',
                        'created_at',
                        [
                            'label' => 'Новая',
                            'value' => function($model) {
                                $result = 'Да';
                                if ($model->is_new == 0) {
                                    $result = 'Нет';
                                }
                                return $result;
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'headerOptions' => ['style' => 'color:#337ab7'],
                            'template' => '{processed}',
                            'buttons' => [
                                'processed' => function ($url, $model) {
                                    return Html::a('Обработана', $url);
                                },

                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                                if ($action === 'processed') {
                                    return Url::to(['/admin/user-request-process', 'id' => $model->id]);
                                }
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
