<?php

/** @var $this \yii\web\View*/
/** @var $user_request \app\models\form\UserRequestForm*/
/** @var $number_new_requests integer*/
/** @var $number_visitors_per_day integer*/


use \yii\helpers\Html;

$this->title = '"OOO" Дом-Сервис';

\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => '"ООО" Дом-Сервис - ',
]);

\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Жалюзи Барнаул, жалюзи барнаул, жалюзи барнаул цена, лучшие жалюзи барнаул, дешевые жалюзи Барнаул, горизонтальные жалюзи барнаул, Москитные сетки Барнаул, москитные сетки барнаул, москитные сетки барнаул цена',
]);

?>

<!-- for gluing together -->
<section id="home"></section>
<div class="wrapper">
    <?= $this->render('sections/navigation', ['number_new_requests' => $number_new_requests, 'number_visitors_per_day' => $number_visitors_per_day]) ?>
    <?= $this->render('sections/main', ['user_request' => $user_request]) ?>
    <?= $this->render('sections/sale') ?>
    <?= $this->render('sections/production') ?>
    <hr>
    <?= $this->render('sections/why_we') ?>
    <?= $this->render('sections/contact') ?>
    <footer id="footer">
        <div class="container">
            <figure>(C) "ООО" ДОМ-СЕРВИС 2006-<?= date('Y') ?></figure>
        </div>
    </footer>
</div>
