<?php
/**
 * Created: 28.05.18
 */
?>

<section id="sale" class="block">
    <div class="container">
        <header><h3>Праздничная акция</h3></header>
        <?= $this->render('sale/items') ?>
    </div><!-- for gluing together -->
</section><!-- for gluing together -->
