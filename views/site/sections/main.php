<?php
/** @var $this \yii\web\View*/
/** @var $user_request \app\models\form\UserRequestForm*/

use yii\helpers\Html;

?>
<section id="slider">
    <div class="flexslider">
        <ul class="slides">
            <li class="slide"><?= Html::img(Yii::getAlias('@web') .'/img/jaluzi.jpg') ?></li>
        </ul><!-- /.slides -->
    </div><!-- /.flexslider -->
    <div class="slider-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="title">
                        <h2>Лучшие жалюзи в Барнауле!</h2>
                        <figure class="subtitle">Заполните форму и в течение 5 минут наш менеджер свяжется с Вами!</figure>
                    </div><!-- for gluing together -->
                </div><!-- for gluing together -->
                <div class="col-md-4 col-sm-4">
                    <div class="form-slider-wrapper">
                        <?= $this->render('main/_user_request_form', [
                                'user_request' => $user_request,
                            ]) ?>
                    </div><!-- for gluing together -->
                </div><!-- for gluing together -->
            </div><!-- for gluing together -->
        </div><!-- for gluing together -->
    </div><!-- for gluing together -->
</section><!-- for gluing together -->
