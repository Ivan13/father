<?php
/**
 * Created: 20.05.18
 */

use yii\helpers\Html;

$production = [
    [
        'img' => 'sale_07_01_25_t1.jpeg',
        'title' => 'Вертикальные жалюзи',
        'description' => '<dl>
                            <dt>Ткань "Франсе"</dt>
                            <dd><s>1850</s> 1400 руб / м<sup>2</sup></dd>
                            <dt>Ткань "Сорренто"</dt>
                            <dd><s>1850</s> 1400 руб / м<sup>2</sup></dd>
                            <dt>Ткань "Махаон"</dt>
                            <dd><s>1850</s> 1400 руб / м<sup>2</sup></dd>
                            <dt>Ткань "Амелия"</dt>
                            <dd><s>1850</s> 1400 руб / м<sup>2</sup></dd>
                        </dl>',
    ],
    [
        'img' => 'sale_07_01_25_bambuk_panda_t1.jpeg',
        'title' => 'Бамбуковые жалюзи',
        'description' => '<dl>
                            <dt>Панда</dt>
                            <dd><s>2850</s> 2200 руб / м<sup>2</sup></dd>
                            <dt style="visibility: hidden"> - </dt>
                            <dd style="visibility: hidden"> - </dd>
                            <dt style="visibility: hidden"> - </dt>
                            <dd style="visibility: hidden"> - </dd>
                            <dt style="visibility: hidden"> - </dt>
                            <dd style="visibility: hidden"> - </dd>
                        </dl>',
    ],
];

?>

<div class="row">
<?php foreach ($production as $item): ?>
    <div class="col-md-5 col-sm-6">
        <div class="property-wrapper">
            <div class="property">
                <a href="javascript: void(0);" class="image-popup">
                    <div class="image-wrapper sale-image-wrapper">
                        <?= Html::img(Yii::getAlias('@web') .'/img/' . $item['img']) ?>
                    </div>
                </a>
                <header><h4><?= $item['title'] ?></h4></header>
                <div class="description">
                    <?= $item['description'] ?>
                </div><!-- for gluing together -->
            </div><!-- for gluing together -->
        </div><!-- for gluing together -->
    </div><!-- for gluing together -->
<?php endforeach; ?>
</div><!-- for gluing together -->
