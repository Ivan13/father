<?php
/**
 * Created: 20.05.18
 */

use yii\helpers\Html;

$production = [
    [
        'img' => 'gorizontal.jpg',
        'title' => 'Горизонтальные',
        'description' => '<dl>
                            <dt>Белые</dt>
                            <dd>от 1750 руб / м<sup>2</sup></dd>
                            <dt>Цветные</dt>
                            <dd>от 2700 руб / м<sup>2</sup></dd>
                        </dl>',
    ],
    [
        'img' => 'vertical.jpg',
        'title' => 'Вертикальные',
        'description' => '<dl>
                            <dt>Эконом</dt>
                            <dd>от 1400 руб / м<sup>2</sup></dd>
                            <dt>Элит</dt>
                            <dd>от 1700 руб / м<sup>2</sup></dd>
                        </dl>',
    ],
    [
        'img' => 'rulon-shtora.jpg',
        'title' => 'Рулонная штора',
        'description' => '<dl>
                            <dt>Blackout</dt>
                            <dd>от 1500 руб за створку</dd>
                            <dt>Затеняющие</dt>
                            <dd>от 1300 руб</dd>
                        </dl>',
    ],
    [
        'img' => 'rol_stavni.jpg',
        'title' => 'Рольставни',
        'description' => '<dl>
                            <dt>Белые</dt>
                            <dd><i>цена договорная</i></dd>
                            <dt>Цветные</dt>
                            <dd><i>цена договорная</i></dd>
                        </dl>',
    ],
    [
        'img' => 'garagnie_vorota.jpg',
        'title' => 'Гаражные ворота',
        'description' => '<dl>
                            <dt>Aлютех</dt>
                            <dd><i>цена договорная</i></dd>
                            <dt>Damast</dt>
                            <dd><i>цена договорная</i></dd>
                        </dl>',
    ],
    [
        'img' => 'setka.jpg',
        'title' => 'Москитные сетки',
        'description' => '<dl>
                            <dt>Ремонт</dt>
                            <dd>450 руб</dd>
                            <dt>Изготовление</dt>
                            <dd>от 1000 руб</dd>
                        </dl>',
    ],
    [
        'img' => 'balcon.jpeg',
        'title' => 'Остекление балконов и лоджий',
        'description' => '<dl>
                            <dt>Из ПВХ или Алюминия</dt>
                            <dd style="visibility: hidden"> - </dd>
                        </dl>',
    ],
    [
        'img' => 'bambuk.jpg',
        'title' => 'Бамбуковые',
        'description' => '<dl>
                            <dt>Изготовление</dt>
                            <dd>от 2850 руб/ м<sup>2</sup></dd>
                        </dl>',
    ],
    [
        'img' => 'minikaseta.jpg',
        'title' => 'Миникасеты',
        'description' => '<dl>
                            <dt>Изготовление</dt>
                            <dd>от 1850 руб</dd>
                        </dl>'
    ]
];

?>

<div class="row">
<?php foreach ($production as $item): ?>
    <div class="col-md-4 col-sm-4">
        <div class="property-wrapper">
            <div class="property">
                <a href="javascript: void(0);" class="image-popup">
                    <div class="image-wrapper">
                        <?= Html::img(Yii::getAlias('@web') .'/img/' . $item['img']) ?>
                    </div>
                </a>
                <header><h4><?= $item['title'] ?></h4></header>
                <div class="description">
                    <?= $item['description'] ?>
                </div><!-- for gluing together -->
            </div><!-- for gluing together -->
        </div><!-- for gluing together -->
    </div><!-- for gluing together -->
<?php endforeach; ?>
</div><!-- for gluing together -->
