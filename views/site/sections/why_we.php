<?php
/** @var $this \yii\web\View*/

use yii\helpers\Html;

?>

<section id="features" class="block">
    <div class="container">
        <header><h3>Почему мы?</h3></header>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <ul class="features-list">
                    <li>1. Доступные цены</li>
                    <li>2. Высокое качество</li>
                    <li>3. Квалифицированные специалисты</li>
                    <li>4. Огромный выбор</li>
                    <li>5. Безупречная репутация компании</li>
                </ul><!-- for gluing together -->
            </div><!-- for gluing together -->
            <div class="col-md-8 col-sm-8">
                <div class="image-carousel">
                    <div class="image-carousel-slide">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <?= Html::img(Yii::getAlias('@web') .'/img/gramota.jpeg', ['style' => 'display: none;']) ?>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <h4>Отзывы</h4>
                                <p><b>Виктор:</b>
                                    <?= $this->render('why_we/starts', ['count' => 5]) ?>
                                    <br>
                                    Индивидуальный подход, цена ниже, чем у других
                                </p>
                                <p><b>Инна:</b>
                                    <?= $this->render('why_we/starts', ['count' => 5]) ?>
                                    <br>
                                    Настоящие профессионалы, качественная установка
                                </p>
                                <p><b>Андрей:</b>
                                    <?= $this->render('why_we/starts', ['count' => 5]) ?>
                                    <br>
                                    Мы обратились в "ООО" Дом-сервис по рекомендации нашего знакомого, наш заказ был
                                    непростой, мало кто готов был браться, но ребята справились! Большое спасибо за профессионализм!
                                </p>
                                <iframe src="https://www.yandex.ru/sprav/widget/rating-badge/1095464614?type=rating&theme=dark" width="150" height="50" frameborder="0"></iframe>
                            </div>
                        </div><!-- for gluing together -->
                    </div><!-- for gluing together -->
                </div><!-- for gluing together -->
            </div><!-- for gluing together -->
        </div><!-- for gluing together -->
    </div><!-- for gluing together -->
</section><!-- for gluing together -->
