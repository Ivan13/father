<?php
/**
 * Created: 28.05.18
 */
?>

<section id="properties" class="block">
    <div class="container">
        <header><h3>Продукция</h3></header>
        <?= $this->render('production/items') ?>
    </div><!-- for gluing together -->
</section><!-- for gluing together -->
