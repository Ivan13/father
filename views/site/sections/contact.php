<?php
/**
 * Created: 28.05.18
 */

use yii\helpers\Html;

?>
<section id="contact" class="block background-color-grey-dark has-dark-background">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <header><h3>Контакты</h3></header>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <address>
                            <strong>График работы:</strong><br>
                            Пн: 09:00 - 18.00 <br>
                            Вт: 09:00 - 18.00 <br>
                            Ср: 09:00 - 18.00 <br>
                            Чт: 09:00 - 18.00 <br>
                            Пт: 09:00 - 18.00 <br>
                            Сб: выходной <br>
                            Вс: выходной <br>
                        </address>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <address>
                            <strong>"ООО" Дом-Сервис</strong><br>
                            Северо-Западная, 39 <br>
                            Барнаул<br>
                            <abbr title="Телефон">Т:</abbr> +7 (3852) 362‒313 <br>
                            <abbr title="Телефон">Т:</abbr> +7 983 608-90-83 <br>
                            <abbr title="Email">E:</abbr> <a href="mailto:triotaln@mail.ru">triotalan@mail.ru</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8">
                <?= Html::img(Yii::getAlias('@web') .'/img/location.png', ['class' => 'img-fluid img-responsive']) ?>
            </div>
        </div>
    </div>
</section>
