<?php

/** @var $number_new_requests integer*/
/** @var $number_visitors_per_day integer*/

use yii\helpers\Html;

?>

<div class="navigation fixed" style="position: fixed !important;">
    <div class="container">
        <header class="navbar" id="top" role="banner">
            <div class="navbar-header">
                <button id="js-mobile-menu" class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Показать меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand nav" id="brand">
                    "ООО" Дом-сервис
                    <?php if ($number_visitors_per_day !== false): ?>
                        <span class="label label-default"><?= $number_visitors_per_day ?></span>
                        <span class="label label-warning"><?= $number_new_requests ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                <ul id="js-nav" class="nav navbar-nav">
                    <li class="active"><a href="#home">Главная</a></li>
                    <li><a href="#sale">Акция</a></li>
                    <li><a href="#properties">Продукция</a></li>
                    <li><a href="#features">Почему мы</a></li>
                    <li><a href="#contact">Контакты</a></li>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <li><?= Html::a('Запросы', ['/admin/user-request']) ?></li>
                        <li><?= Html::a('Выйти', ['/admin/logout'], ['data' => ['method' => 'post']]) ?></li>
                    <?php endif; ?>
                </ul>
            </nav><!-- for gluing together -->
        </header><!-- for gluing together -->
    </div><!-- for gluing together -->
</div><!-- for gluing together -->
