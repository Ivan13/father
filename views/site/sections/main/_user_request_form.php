<?php

/** @var $user_request \app\models\form\UserRequestForm*/

use \yii\helpers\Html;
?>

<?= Html::beginForm(
        ['form/user-request'],
        'post',
        ['id' => 'form-slider', 'data-sform' => 1, 'data-submit-btn' => '#form-slider-submit']
); ?>
    <header>
        <h3>Обратный звонок</h3>
        <span class="success-form"></span>
        <span class="error-form"></span>
    </header>
    <hr>
    <div class="form-group">
        <label for="name"><?= $user_request->getAttributeLabel('name') ?><em>*</em></label>
        <?= Html::activeInput(
            'text',
            $user_request,
            'name',
            ['id' => 'name', 'class' => 'form-control']
        )?>
    </div><!-- for gluing together -->
    <div class="form-group">
        <label for="email"><?= $user_request->getAttributeLabel('phone') ?><em>*</em></label>
        <?= Html::activeInput(
            'text',
            $user_request,
            'phone',
            ['id' => 'phone', 'class' => 'form-control']
        ) ?>
    </div><!-- for gluing together -->
    <div class="form-group">
        <label for="phone"><?= $user_request->getAttributeLabel('email') ?></label>
        <?= Html::activeInput(
            'text',
            $user_request,
            'email',
            ['id' => 'email', 'class' => 'form-control']
        ) ?>
    </div><!-- for gluing together -->
    <div class="form-group">
        <div id="form-slider-status"></div>
        <?= Html::submitButton(
            $user_request->getAttributeLabel('submit'),
            ['id' => 'form-slider-submit', 'class' => 'btn btn-default']
        ) ?>
    </div><!-- for gluing together -->
    <figure>*Обязательные поля</figure>
<?= Html::endForm() ?>
